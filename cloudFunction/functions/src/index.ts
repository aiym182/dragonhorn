import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp(functions.config().firebase);


const fcm  =admin.messaging();


export const helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});

export const upperTaylorWarning = functions.https.onCall((data,context)=> {


  const payload : admin.messaging.MessagingPayload = {

    notification:{
      title: 'Dragon Alert!!!!',
      body: 'Dragon Appeared in Taylor - Upper !!!!'
    }
  }
   return admin.messaging.call(payload);
});
export const offLine = functions.https.onCall((data,context)=>{

      if(context.auth){
        const  uid = context.auth.uid;
        return admin.firestore().collection('profile').doc(uid).update({
          'SomOnline' : false,
          'SovOnline' : false,
          'UgOnline'  : false,
      });
  
        
      }
      else{

        return ;
      }

  
    
  

});

