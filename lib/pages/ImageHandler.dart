import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_storage/firebase_storage.dart';

class ImageHandler with ChangeNotifier {
  ImageHandler();
  File _file;
  String picUrl;
  String onlinePicUrl;
  Firestore db = Firestore.instance;
  bool updated = false;

  final FirebaseStorage storage =
      FirebaseStorage(storageBucket: 'gs://dragonhorn-462df.appspot.com');

  getFile() => _file = _file;
  setFile(File file) {
    _file = file;
    notifyListeners();
  }

  setUpdated(bool update) {
    updated = update;
    notifyListeners();
  }

  getUrl() => picUrl = picUrl;

  setUrl(String _picUrl) {
    picUrl = _picUrl;
    notifyListeners();
  }

  Future<void> uploadImageAndUploadFromCamera(FirebaseUser user) async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxWidth: 400,
      maxHeight: 400,
    );
    _file = image;
    uploadImageToFirebaseStorage(_file, user);
  }

  Future<void> uploadImageAndUploadFromGallary(FirebaseUser user) async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 400, maxHeight: 400);
    _file = image;
    uploadImageToFirebaseStorage(_file, user);
  }

  Future<void> deleteImage(FirebaseUser user) async {
    if (picUrl == null) {
    } else {
      storage
          .getReferenceFromUrl(picUrl)
          .then((value) => value.delete())
          .catchError(
            (e) => print(e),
          );
    }
  }

  uploadImageToFirebaseStorage(File file, FirebaseUser user) {
    StorageUploadTask _uploadTask;
    String filepath = 'images/${DateTime.now()}.png';

    if (file != null) {
      if (picUrl != null) {
        try {
          storage.getReferenceFromUrl(picUrl).then((value) {
            value.delete().whenComplete(() {
              _uploadTask = storage.ref().child(filepath).putFile(file);
              _uploadTask.onComplete.then((value) {
                value.ref.getDownloadURL().then((s) {
                  if (s != null) {
                    setUpdated(false);
                    setUrl(s);
                    setPictureUrlToFirebase(user, s);
                    setPictureUrlToLocalData(s);
                  } else {
                    print('connection error');
                  }
                });
              });
            }).catchError((e) {
              _uploadTask = storage.ref().child(filepath).putFile(file);
              _uploadTask.onComplete.then((value) {
                value.ref.getDownloadURL().then((s) {
                  if (s != null) {
                    setUpdated(false);
                    setUrl(s);
                    setPictureUrlToFirebase(user, s);
                    setPictureUrlToLocalData(s);
                  } else {
                    print('connection error');
                  }
                });
              });
            });
          });
        } catch (e) {
          e.toString();
        }
      } else {
        _uploadTask = storage.ref().child(filepath).putFile(file);
        _uploadTask.onComplete.then((value) {
          value.ref.getDownloadURL().then((s) {
            if (s != null) {
              setUpdated(false);
              setUrl(s);
              setPictureUrlToFirebase(user, s);
              setPictureUrlToLocalData(s);
            } else {
              print('connection error');
            }
          });
        });
      }
    } else {
      print("user canceled selection");
    }
  }

  // fetching picurl and delete from storage
  Future<void> deleteUrlAndImage(FirebaseUser user) async {
    var url;
    await getOnlinePictureUrl(user).then((value) async {
      url = value;

      if (url != null)
        await storage.getReferenceFromUrl(url).then((pt) async {
          pt.delete();
        });
      else {}
    });
  }

  Future<void> setPictureUrlToLocalData(String url) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('pictureUrl', url);
  }

  Future<void> getAndSetLocalPictureUrl(FirebaseUser user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    picUrl = pref.getString('pictureUrl');
  }

  Future<String> getOnlinePictureUrl(FirebaseUser user) async {
    DocumentSnapshot doc =
        await db.collection('profile').document(user.uid).get();

    return doc.data['pictureUrl'];
  }

  void setPictureUrlToFirebase(FirebaseUser user, var url) async {
    try {
      await db
          .collection('profile')
          .document(user.uid)
          .updateData({"pictureUrl": url});
    } catch (e) {
      print(e.toString());
    }
  }
}
