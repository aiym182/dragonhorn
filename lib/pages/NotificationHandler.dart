import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/cupertino.dart';

class NotificationHandler with ChangeNotifier {
  NotificationHandler();

  Firestore fs = Firestore.instance;
  CloudFunctions cf = CloudFunctions.instance;

  void setTime(String buildingName) {
    fs.collection('notification').document(buildingName).get().then((value) {
      if (value.exists) {
        DocumentReference ref = value.reference;

        fs.runTransaction((Transaction tran) async {
          await tran.update(ref, {'TimeAlarmed': DateTime.now()});
        });
      } else {
        fs
            .collection('notification')
            .document(buildingName)
            .setData({'TimeAlarmed': DateTime.now()});
      }
    });
  }

  Future<int> isAlarmPossible(String buildingName) async {
    DateTime lastTime;
    int duration;
    await fs
        .collection('notification')
        .document(buildingName)
        .get()
        .then((value) {
      if (value.exists) {
        if (value.data['TimeAlarmed'] == null) {
          lastTime = DateTime.now();
          duration = 300;
        } else {
          lastTime = value.data['TimeAlarmed'].toDate();
          duration = DateTime.now().difference(lastTime).inSeconds;
        }
      } else {
        lastTime = DateTime.now();
        duration = 300;
      }
    });

    return duration;
  }
}
