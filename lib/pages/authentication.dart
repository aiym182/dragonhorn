import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_storage/firebase_storage.dart';

class UserAuth with ChangeNotifier {
  FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseUser user;
  FirebaseStorage db = FirebaseStorage.instance;
  Firestore fs = Firestore.instance;

  Future<FirebaseUser> signIn(String email, String password) async {
    var user;
    await auth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) async {
      user = value.user;
    });
    return user;
  }

  Future<void> signOut(BuildContext context) async {
    await auth.signOut();
  }

  Future<void> resetPassword(String email) async {
    await auth.sendPasswordResetEmail(email: email);
  }

  Future<void> deleteAccount(BuildContext context) async {
    FirebaseUser _firebaseUser = await auth.currentUser();

    await _firebaseUser.delete();
  }

  Future<FirebaseUser> userRegistration(String email, password, name) async {
    AuthResult user = await auth.createUserWithEmailAndPassword(
        email: email, password: password);

    return user.user;
  }

  Future<void> refresh() async {
    await auth
        .currentUser()
        .then((value) => value.reload().then((value) => auth.currentUser()));
  }

  Future<FirebaseUser> currentUserInfo() async {
    user = await auth.currentUser();

    return user;
  }
}
