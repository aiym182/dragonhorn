import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dragonhorn/pages/authentication.dart';
import 'package:dragonhorn/pages/signup.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String email, password, finalMessage;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[logInScreenBody()],
      ),
    );
  }

  Widget message(String message) {
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    if (message != null) {
      return Text(message,
          style: TextStyle(fontSize: 15 * scaleFactor, color: Colors.red));
    } else {
      return Text('',
          style: TextStyle(fontSize: 15 * scaleFactor, color: Colors.red));
    }
  }

  bool emailValidator(String email) {
    Pattern pat =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pat);
    if (email == null || !regExp.hasMatch(email)) {
      return false;
    } else {
      return true;
    }
  }

  bool passWordValidator(String password) {
    if (password == null || password.length < 8 || password.length > 16) {
      return false;
    } else {
      return true;
    }
  }

  String finalValidationMessage(String email, password) {
    if (!emailValidator(email)) {
      return "Email format is not valid.";
    } else if (!passWordValidator(password)) {
      return 'Your password must be in between 8~15 characters.';
    } else
      return '';
  }

  Widget logInScreenBody() {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    return Scaffold(
      body: Form(
        child: Container(
          color: Theme.of(context).backgroundColor,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset('images/horn.png',
                      height: size.width * .2, width: size.width * .2),
                  SizedBox(height: size.height * .05),
                  Container(
                    width: size.width * .6,
                    height: size.height * .07,
                    child: TextField(
                      style: TextStyle(color: Colors.black),
                      cursorColor: Theme.of(context).cursorColor,
                      textAlignVertical: TextAlignVertical.bottom,
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                          color: Colors.deepOrange[900],
                        )),
                        hintText: 'Email',
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) {
                        this.setState(() {
                          email = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(height: 5),
                  Container(
                    width: size.width * .6,
                    height: size.height * .07,
                    child: TextField(
                      style: TextStyle(color: Colors.black),
                      cursorColor: Theme.of(context).cursorColor,
                      textAlignVertical: TextAlignVertical.bottom,
                      autocorrect: false,
                      obscureText: true,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Password',
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                          color: Colors.deepOrange[900],
                        )),
                        border: OutlineInputBorder(),
                      ),
                      onChanged: (value) {
                        this.setState(() {
                          password = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(height: size.height * .02),
                  Container(
                    width: size.width * .6,
                    height: size.height * .07,
                    color: Theme.of(context).buttonColor,
                    child: FlatButton(
                      child: Text(
                        'Log In',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17 * scaleFactor,
                        ),
                      ),
                      onPressed: () async {
                        if (emailValidator(email) &&
                            passWordValidator(password)) {
                          Provider.of<UserAuth>(context)
                              .signIn(email, password)
                              .then((value) {
                            if (!value.isEmailVerified) {
                              value.sendEmailVerification().whenComplete(() {
                                EmailVerificationMessage(context, value.email);
                              });
                            } else {}
                          }).catchError(
                            (e) {
                              setState(() {
                                finalMessage = e.message.toString();
                              });
                            },
                          );
                        } else {
                          setState(() {
                            finalMessage =
                                finalValidationMessage(email, password);
                          });
                        }
                      },
                    ),
                  ),
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: size.height * .02, bottom: size.height * .01),
                        child: Text(
                          'If you are not a member, Please',
                          style: TextStyle(fontSize: 15 * scaleFactor),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: size.height * .01,
                            top: size.height * .02,
                            left: size.height * .02,
                            right: size.height * .02),
                        child: GestureDetector(
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                fontSize: 15 * scaleFactor,
                                color: Theme.of(context).accentColor,
                                decoration: TextDecoration.underline,
                                decorationColor: Theme.of(context).accentColor,
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).pushNamed('signUp');
                            }),
                      ),
                    ],
                  )),
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Trouble logging in? ',
                        style: TextStyle(fontSize: 15 * scaleFactor),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: size.height * .02),
                        child: GestureDetector(
                            child: Text(
                              'Reset Password',
                              style: TextStyle(
                                fontSize: 15 * scaleFactor,
                                color: Theme.of(context).accentColor,
                                decoration: TextDecoration.underline,
                                decorationColor: Theme.of(context).accentColor,
                              ),
                            ),
                            onTap: () {
                              ResetPassword(context);
                            }),
                      ),
                    ],
                  )),
                  SizedBox(height: size.height * .04),
                  Container(
                    width: size.width * .6,
                    alignment: Alignment.center,
                    child: message(finalMessage),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ResetPassword {
  @override
  ResetPassword(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    TextEditingController textControl = TextEditingController();

    bool emailValidator(String email) {
      Pattern pat =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = RegExp(pat);
      if (email == null || !regExp.hasMatch(email)) {
        return false;
      } else {
        return true;
      }
    }

    showDialog(
        context: context,
        builder: (context) {
          void callback() {
            if (emailValidator(textControl.text)) {
              Provider.of<UserAuth>(context)
                  .resetPassword(textControl.text)
                  .whenComplete(() {
                Navigator.of(context).pop();

                String emailAddress = textControl.text;
                finalMessage(
                    context,
                    "Check your Email",
                    "Email has been sent to $emailAddress. " +
                        "\n" +
                        '\n' +
                        "Please check your email.");
              }).catchError((e) {
                Navigator.of(context).pop();
                finalMessage(context, 'Uh Oh..', e.message.toString());
              });
            } else {
              Navigator.of(context).pop();
              finalMessage(
                  context,
                  'Uh Oh ..',
                  'Your email format is not valid.' +
                      "\n" +
                      "\n" +
                      'Please try again.');
            }
          }

          return Dialog(
            elevation: 5,
            backgroundColor: Theme.of(context).backgroundColor,
            child: Container(
              width: size.width * .6,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                      vertical: size.height * .005,
                    ),
                    color: Theme.of(context).cardColor,
                    width: double.infinity,
                    child: Text(
                      'Reset Password',
                      style: TextStyle(
                          color: Colors.white, fontSize: 30 * scaleFactor),
                    ),
                  ),
                  SizedBox(height: size.height * .03),
                  Container(
                    width: size.width * .6,
                    height: size.height * .07,
                    alignment: Alignment.center,
                    child: TextField(
                      onSubmitted: (value) => callback(),
                      controller: textControl,
                      style: TextStyle(color: Colors.black),
                      cursorColor: Theme.of(context).cursorColor,
                      textAlignVertical: TextAlignVertical.bottom,
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                          color: Colors.deepOrange[900],
                        )),
                        hintText: 'Email',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    color: Theme.of(context).backgroundColor,
                    width: double.infinity,
                    padding: EdgeInsets.only(
                      right: size.width * .05,
                      left: size.width * .05,
                      top: size.height * .03,
                      bottom: size.height * .03,
                    ),
                    child: Text(
                      "Enter your email to send a link to reset your account password.",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 15 * scaleFactor),
                    ),
                  ),
                  Container(
                    width: size.width * .4,
                    height: size.height * .06,
                    color: Theme.of(context).buttonColor,
                    child: FlatButton(
                      child: Text(
                        'Send',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17 * scaleFactor,
                        ),
                      ),
                      onPressed: () async {
                        callback();
                      },
                    ),
                  ),
                  SizedBox(height: size.height * .03),
                ],
              ),
            ),
          );
        });
  }

  void finalMessage(BuildContext context, String title, String body) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5,
            backgroundColor: Theme.of(context).backgroundColor,
            child: Container(
              width: size.width * .6,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                      vertical: size.height * .005,
                    ),
                    color: Theme.of(context).cardColor,
                    width: double.infinity,
                    child: Text(
                      title,
                      style: TextStyle(
                          color: Colors.white, fontSize: 30 * scaleFactor),
                    ),
                  ),
                  SizedBox(height: size.height * .03),
                  Container(
                    alignment: Alignment.center,
                    color: Theme.of(context).backgroundColor,
                    width: double.infinity,
                    padding: EdgeInsets.only(
                      right: size.width * .05,
                      left: size.width * .05,
                      top: size.height * .03,
                      bottom: size.height * .03,
                    ),
                    child: Text(
                      body,
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 15 * scaleFactor),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        color: Colors.transparent,
                        alignment: Alignment.center,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: size.width * .08,
                              vertical: size.height * .03),
                          child: Text('Ok',
                              style: TextStyle(
                                fontSize: 20 * scaleFactor,
                                color: Theme.of(context).accentColor,
                              )),
                        )),
                  )
                ],
              ),
            ),
          );
        });
  }
}
