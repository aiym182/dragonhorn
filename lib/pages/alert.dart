import 'package:flutter/material.dart';
import 'package:dragonhorn/pages/NotificationHandler.dart';
import 'package:provider/provider.dart';
import 'package:cloud_functions/cloud_functions.dart';

class AlertScreen extends StatefulWidget {
  @override
  _AlertScreenState createState() => _AlertScreenState();
}

class _AlertScreenState extends State<AlertScreen> {
  final buildingNames = [
    'Taylor - Upper',
    'Taylor - Lower',
    'Modica - Upper',
    'Modica - Lower South',
    'Modica - Lower North',
    'Belford - 2nd Floor',
    'Belford - 3rd Floor',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView.separated(
          itemCount: buildingNames.length,
          itemBuilder: (context, index) {
            return ListTile(
              contentPadding: EdgeInsets.only(
                top: size.height * .015,
                bottom: size.height * .015,
                left: size.width * .1,
              ),
              title: Text(
                buildingNames[index],
                style: TextStyle(
                    fontSize: 25 * scaleFactor,
                    color: Theme.of(context).primaryColor),
              ),
              subtitle: Padding(
                  padding: EdgeInsets.symmetric(vertical: size.height * .010),
                  child: Text(
                    "Push to notify everyone",
                    style: TextStyle(
                        color: Colors.redAccent,
                        fontSize: 15 * scaleFactor,
                        fontWeight: FontWeight.w300),
                  )),
              onTap: () {
                Provider.of<NotificationHandler>(context)
                    .isAlarmPossible(buildingNames[index])
                    .then((duration) {
                  onButtonPushed(index, duration);
                });
              },
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        ),
      ),
    );
  }

  void onButtonPushed(int index, int duration) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;
    int timeleft;

    setState(() {
      timeleft = 300 - duration;
    });

    // Making button for pop ups

    Widget okButton = FlatButton(
      child: Text('Yes',
          style: TextStyle(
              color: Colors.deepOrange[900], fontSize: 20 * scaleFactor)),
      onPressed: () {
        final HttpsCallable callable = CloudFunctions.instance
            .getHttpsCallable(functionName: 'dragonNotification');
        callable.call(<String, dynamic>{
          'buildingNames': buildingNames[index],
        });

        Provider.of<NotificationHandler>(context).setTime(buildingNames[index]);
        Navigator.of(context).pop();
      },
    );
    Widget noButton = FlatButton(
      child: Text('No',
          style: TextStyle(
              color: Colors.deepOrange[900], fontSize: 20 * scaleFactor)),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // Dialog when is possible to notify
    Dialog alert = Dialog(
      backgroundColor: Theme.of(context).backgroundColor,
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: size.height * .005),
              alignment: Alignment.center,
              color: Theme.of(context).cardColor,
              width: double.infinity,
              child: Text(
                'Alert Everyone?',
                style:
                    TextStyle(color: Colors.white, fontSize: 30 * scaleFactor),
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: double.infinity,
              padding: EdgeInsets.only(
                right: size.width * .08,
                left: size.width * .08,
                top: size.height * .03,
                bottom: size.height * .03,
              ),
              child: Text(
                buildingNames[index],
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 20 * scaleFactor),
              ),
            ),
            ButtonBar(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                okButton,
                noButton,
              ],
            ),
          ],
        ),
      ),
    );

    // dialog when it is not possible to notify
    Dialog noAlert = Dialog(
      backgroundColor: Colors.black,
      child: Container(
        color: Theme.of(context).backgroundColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                padding: EdgeInsets.symmetric(vertical: size.height * .005),
                alignment: Alignment.center,
                color: Theme.of(context).cardColor,
                width: double.infinity,
                child: Text(
                  'Try again',
                  style: TextStyle(
                      color: Colors.white, fontSize: 30 * scaleFactor),
                )),
            Container(
              color: Theme.of(context).backgroundColor,
              alignment: Alignment.center,
              width: double.infinity,
              padding: EdgeInsets.only(
                right: size.width * .08,
                left: size.width * .08,
                top: size.height * .03,
                bottom: size.height * .01,
              ),
              child: Text(
                'It has been alarmed already.' +
                    "\n" +
                    'Please try again later.',
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 18 * scaleFactor),
              ),
            ),
            Container(
              color: Theme.of(context).backgroundColor,
              alignment: Alignment.center,
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(
                  right: size.width * .08,
                  left: size.width * .08,
                  top: size.height * .03,
                ),
                child: Text('(' + timeleft.toString() + ' seconds left )',
                    style: TextStyle(
                        color: Colors.grey[600], fontSize: 15 * scaleFactor)),
              ),
            ),
            FlatButton(
              child: Text('Ok',
                  style: TextStyle(
                      color: Colors.deepOrange[900],
                      fontSize: 20 * scaleFactor)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );

    //Showing Dialog
    showDialog(
        context: context,
        builder: (BuildContext context) {
          if (duration >= 300) {
            return alert;
          } else {
            return noAlert;
          }
        });
  }
}
