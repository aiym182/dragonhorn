import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OnLineList extends StatelessWidget {
  final String picUrl;
  final String username;
  final bool isMe;

  const OnLineList({Key key, this.picUrl, this.username, this.isMe})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    ImageProvider listImg(String pic) {
      if (pic == null) {
        return AssetImage('images/horn_ios.png');
      } else {
        return CachedNetworkImageProvider(pic, errorListener: () {});
      }
    }

    return ListTile(
      onTap: () {
        userProfile(context, username, picUrl);
      },
      leading: CircleAvatar(
        backgroundImage: listImg(picUrl),
        backgroundColor: Colors.deepOrange[900],
      ),
      title: Text(
        username,
        style: TextStyle(
          fontSize: 16 * scaleFactor,
          color: isMe ? Colors.greenAccent : Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}

void userProfile(BuildContext context, String userName, String pic) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        Size size = MediaQuery.of(context).size;
        final scaleFactor = MediaQuery.of(context).textScaleFactor;

        Widget image(String picUrl) {
          if (picUrl == null) {
            return Image(
              image: AssetImage(
                'images/horn_ios.png',
              ),
            );
          } else {
            return CachedNetworkImage(
              imageUrl: picUrl,
              errorWidget: (context, url, error) =>
                  Image.asset('images/horn_ios.png'),
            );
          }
        }

        return Dialog(
          child: Container(
            width: size.width * .66,
            color: Theme.of(context).cardColor,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  image(pic),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: size.height * .02),
                    child: Text(
                      userName,
                      style: TextStyle(
                          fontSize: 25 * scaleFactor, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      });
}
