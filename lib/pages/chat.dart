import 'package:dragonhorn/pages/chatRoom.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatelessWidget {
  final FirebaseUser user;
  final String username;
  final String picUrl;

  const ChatScreen({Key key, this.user, this.username, this.picUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: ListView(
        children: ListTile.divideTiles(
          color: Theme.of(context).dividerColor,
          context: context,
          tiles: [
            ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              leading: Icon(Icons.chat_bubble_outline,
                  color: Theme.of(context).primaryColor, size: 30),
              title: Text(
                "SGU - SOM",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 25),
              ),
              subtitle: Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Chat room for SGU School of Medicine students',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorDark,
                    fontSize: 15,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => ChatRoom(
                        user: user,
                        username: username,
                        picUrl: picUrl,
                        chatRoomPath: 'som',
                        pageTitle: "SOM ChatRoom"),
                  ),
                );
              },
            ),
            ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              leading: Icon(Icons.chat_bubble_outline,
                  color: Theme.of(context).primaryColor, size: 30),
              title: Text(
                "SGU - SOV",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 25),
              ),
              subtitle: Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Chat room for SGU school of Veterinary medicine students',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorDark,
                    fontSize: 15,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => ChatRoom(
                        user: user,
                        username: username,
                        picUrl: picUrl,
                        chatRoomPath: 'sov',
                        pageTitle: "SOV ChatRoom"),
                  ),
                );
              },
            ),
            ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              leading: Icon(Icons.chat_bubble_outline,
                  color: Theme.of(context).primaryColor, size: 30),
              title: Text(
                "SGU - Undergraduate",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 25),
              ),
              subtitle: Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Chat room for SGU Undergraduate students',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorDark,
                    fontSize: 15,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => ChatRoom(
                        user: user,
                        username: username,
                        picUrl: picUrl,
                        chatRoomPath: 'ug',
                        pageTitle: "UG ChatRoom"),
                  ),
                );
              },
            ),
          ],
        ).toList(),
      ),
    );
  }
}
