import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OptionProvider with ChangeNotifier {
  OptionProvider(this._theme);

  ThemeData _theme;

  getTheme() => _theme;

  setTheme(ThemeData theme) async {
    _theme = theme;
    notifyListeners();
  }

  Future<void> saveNotiSound(bool sound) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool('NotiSound', sound);
  }
}
