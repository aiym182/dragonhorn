import 'package:dragonhorn/pages/ImageHandler.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dragonhorn/pages/StorageBackEnd.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProfileSetting extends StatefulWidget {
  @override
  _ProfileSettingState createState() => _ProfileSettingState();
  final FirebaseUser user;

  const ProfileSetting({Key key, this.user}) : super(key: key);
}

class _ProfileSettingState extends State<ProfileSetting> {
  String newUserName = '';
  String oldUserName = '';
  String warningMessage = '';

  @override
  Widget build(BuildContext context) {
    oldUserName = Provider.of<FirebaseBackEnd>(context).getName();

    var _imageHandler = Provider.of<ImageHandler>(context);
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    bool usernameValidator(String username) {
      if (username == null || username.length < 3 || username.length > 16) {
        return false;
      } else {
        return true;
      }
    }

    ImageProvider image() {
      if (_imageHandler.picUrl == null) {
        return AssetImage('images/horn.png');
      } else {
        return CachedNetworkImageProvider(_imageHandler.picUrl);
      }
    }

    validation() {
      if (usernameValidator(newUserName)) {
        Provider.of<FirebaseBackEnd>(context)
            .updateUserName(widget.user, newUserName);
        Provider.of<FirebaseBackEnd>(context)
            .setLocalUserData(widget.user.email, newUserName);
        Provider.of<FirebaseBackEnd>(context).setName(newUserName);

        setState(() {
          warningMessage = "";
        });
      } else {
        setState(() {
          warningMessage = "Your username must be in between 3~15 characters.";
        });
      }
    }

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            'Profile',
            style: TextStyle(fontSize: 20 * scaleFactor, color: Colors.white),
          ),
          backgroundColor: Theme.of(context).cardColor,
          elevation: 5,
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  fit: StackFit.loose,
                  children: <Widget>[
                    Container(
                      width: size.width * .5,
                      height: size.width * .5,
                      child: CircleAvatar(
                        backgroundImage: image(),
                        backgroundColor: Colors.grey,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          uploadImageDialog(context, widget.user);
                        },
                        child: Container(
                            width: size.width * .13,
                            height: size.width * .13,
                            child: Container(
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.deepOrange[900]),
                                  shape: BoxShape.circle,
                                  color: Colors.deepOrange[800]),
                              child:
                                  Icon(Icons.camera_alt, color: Colors.white),
                            )),
                      ),
                    ),
                  ],
                ),
                Padding(
                    child: Text(oldUserName,
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 25 * scaleFactor)),
                    padding: EdgeInsets.only(top: size.height * .04)),
                SizedBox(height: size.height * .04),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: size.width * .5,
                      height: size.height * .06,
                      child: TextField(
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.deepOrange[800],
                        textAlignVertical: TextAlignVertical.center,
                        autocorrect: false,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                            color: Colors.deepOrange[800],
                          )),
                          hintText: 'username',
                          border: OutlineInputBorder(),
                        ),
                        onChanged: (value) {
                          newUserName = value;
                        },
                      ),
                    ),
                    SizedBox(width: size.width * .02),
                    GestureDetector(
                      onTap: () => validation(),
                      child: Container(
                        width: size.height * .06,
                        height: size.height * .06,
                        decoration: BoxDecoration(
                          color: Colors.deepOrange[800],
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                        ),
                        child: Icon(Icons.edit, color: Colors.white),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(
                  alignment: Alignment.center,
                  width: 250,
                  child: Text(warningMessage,
                      style: TextStyle(color: Colors.red, fontSize: 15)),
                ),
              ],
            ),
          ),
        ));
  }
}

void uploadImageDialog(BuildContext context, FirebaseUser user) {
  showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 30),
                  alignment: Alignment.centerLeft,
                  color: Theme.of(context).cardColor,
                  child: Text(
                    'Upload Image',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Provider.of<ImageHandler>(context)
                        .uploadImageAndUploadFromCamera(user);

                    Navigator.of(context).pop();
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    color: Theme.of(context).backgroundColor,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 60, left: 5),
                          child: Icon(Icons.camera_alt,
                              color: Theme.of(context).primaryColor, size: 30),
                        ),
                        Text(
                          'Camera',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Provider.of<ImageHandler>(context)
                        .uploadImageAndUploadFromGallary(user);

                    Navigator.of(context).pop();
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.center,
                    color: Theme.of(context).backgroundColor,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 60, left: 5),
                          child: Icon(Icons.image,
                              color: Theme.of(context).primaryColor, size: 30),
                        ),
                        Text(
                          'Gallary',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      });
}
