import 'package:dragonhorn/pages/onLineList.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dragonhorn/pages/message.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:cloud_functions/cloud_functions.dart';

class ChatRoom extends StatefulWidget {
  const ChatRoom(
      {Key key,
      this.user,
      this.username,
      this.picUrl,
      this.pageTitle,
      this.chatRoomPath})
      : super(key: key);

  @override
  _ChatRoomState createState() => _ChatRoomState();

  final FirebaseUser user;
  final String username;
  final String picUrl;
  final String pageTitle;
  final String chatRoomPath;
}

class _ChatRoomState extends State<ChatRoom> with WidgetsBindingObserver {
  TextEditingController messageController;
  Firestore fs = Firestore.instance;
  ScrollController scrollController;
  ScrollController drawerScroll;
  List<Widget> messageList;
  List<dynamic> mess;

  void online(FirebaseUser user, String path) {
    Firestore.instance.collection('profile').document(user.uid).setData(
      {
        path: true,
      },
      merge: true,
    );
  }

  Future<void> callback() async {
    DateFormat date = DateFormat("yyyy/MM/dd HH:mm:ss");

    Map sendMessage = {
      'text': messageController.text,
      'username': widget.username,
      'email': widget.user.email,
      'timeStamp': date.format(DateTime.now()).toString(),
      'pictureUrl': widget.picUrl,
    };

    if (messageController.text.length > 0) {
      fs
          .collection('chatRoom')
          .document(widget.chatRoomPath)
          .get()
          .then((value) {
        if (!value.exists) {
          fs.collection('chatRoom').document(widget.chatRoomPath).setData({
            'count': FieldValue.increment(1),
            'mess': FieldValue.arrayUnion([sendMessage])
          });
        } else {
          DocumentReference ref = value.reference;
          var docuSnap = ref.get();
          docuSnap.then((value) {
            if (value.data['count'] < 800) {
              fs.runTransaction((Transaction trans) async {
                await trans.update(ref, {
                  'count': FieldValue.increment(1),
                  'mess': FieldValue.arrayUnion([sendMessage])
                });
              });
            } else {
              ref.delete().whenComplete(() async {
                await fs
                    .collection('chatRoom')
                    .document(widget.chatRoomPath)
                    .setData({
                  'count': FieldValue.increment(1),
                  'mess': FieldValue.arrayUnion([sendMessage])
                });
              });
            }
          });
        }
      });
      messageController.clear();
      scrollController.animateTo(
        scrollController.position.minScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  @override
  void initState() {
    online(widget.user, widget.chatRoomPath);
    messageController = TextEditingController();
    scrollController = ScrollController();
    drawerScroll = ScrollController();
    WidgetsBinding.instance.addObserver(this);
//    drawerScroll.addListener();
    scrollController.addListener(scrollListener);

    super.initState();
  }

  @override
  void dispose() {
    final HttpsCallable callable =
        CloudFunctions.instance.getHttpsCallable(functionName: 'offLine');
    callable.call();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      final HttpsCallable callable =
          CloudFunctions.instance.getHttpsCallable(functionName: 'offLine');
      callable.call();

      // app is on background
    }
    if (state == AppLifecycleState.resumed) {
      // app is on foreground
      online(widget.user, widget.chatRoomPath);
    }
    if (state == AppLifecycleState.inactive) {
      // app is inactive
      final HttpsCallable callable =
          CloudFunctions.instance.getHttpsCallable(functionName: 'offLine');
      callable.call();
    }
    if (state == AppLifecycleState.suspending) {
      final HttpsCallable callable =
          CloudFunctions.instance.getHttpsCallable(functionName: 'offLine');
      callable.call();
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      endDrawer: memberList(context),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(widget.pageTitle,
            style: TextStyle(color: Colors.white, fontSize: 20 * scaleFactor)),
        backgroundColor: Theme.of(context).cardColor,
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: chatStream(context),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: size.height * .001,
                bottom: size.height * .001,
              ),
              child: Container(
                color: Theme.of(context).backgroundColor,
                height: size.height * .1,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: size.height * .01,
                            bottom: size.height * .01,
                            left: size.width * .02,
                            right: size.width * .02),
                        child: TextField(
                          textAlignVertical: TextAlignVertical.bottom,
                          enableInteractiveSelection: true,
                          autocorrect: false,
                          autofocus: false,
                          style: TextStyle(color: Colors.black),
                          onSubmitted: (value) => callback(),
                          controller: messageController,
                          cursorColor: Colors.deepOrange[900],
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.deepOrange[900],
                              ),
                            ),
                            border: OutlineInputBorder(),
                            focusColor: Colors.deepOrange[900],
                            hintText: 'Send a message',
                            filled: true,
                            fillColor: Colors.orange[50],
                          ),
                        ),
                      ),
                    ),
                    SendButton(
                      callback: callback,
                    ),
                    SizedBox(width: size.width * .02),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget memberList(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    return SizedBox(
      width: size.width * .6,
      child: Drawer(
        child: Container(
          color: Theme.of(context).backgroundColor,
          child: SafeArea(
            child: MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: StreamBuilder(
                  stream: fs
                      .collection('profile')
                      .where(widget.chatRoomPath, isEqualTo: true)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Colors.deepOrange[900]),
                        ),
                      );
                    } else {
                      if (!snapshot.hasData) {
                        return ListView(
                          children: <Widget>[
                            Container(
                              child: ListTile(
                                  title: Text(
                                'Online',
                                style: TextStyle(
                                  fontSize: 20 * scaleFactor,
                                  color: Colors.white,
                                ),
                              )),
                            ),
                            Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.deepOrange[900]),
                              ),
                            )
                          ],
                        );
                      } else {
                        List<DocumentSnapshot> docs = snapshot.data.documents;
                        List<Widget> userStatus = docs
                            .map((value) => OnLineList(
                                  username: value.data['username'],
                                  picUrl: value.data['pictureUrl'],
                                  isMe:
                                      widget.username == value.data['username'],
                                ))
                            .toList();

                        return ListView(
                          controller: drawerScroll,
                          children: <Widget>[
                            Container(
                              color: Theme.of(context).cardColor,
                              child: ListTile(
                                  title: Text(
                                'Online',
                                style: TextStyle(
                                  fontSize: 20 * scaleFactor,
                                  color: Colors.white,
                                ),
                              )),
                            ),
                            ...userStatus
                          ],
                        );
                      }
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }

  scrollListener() {
//    if (scrollController.offset >= scrollController.position.maxScrollExtent &&
//        !scrollController.position.outOfRange) {}
  }

  Widget chatStream(BuildContext context) {
    return StreamBuilder(
      stream:
          fs.collection('chatRoom').document(widget.chatRoomPath).snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
              child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.deepOrange[900]),
          ));
        } else {
          DocumentSnapshot doc = snapshot.data;

          if (doc.data == null) {
            return ListView(controller: scrollController);
          } else {
            List<Widget> messageList;
            List<dynamic> mess;
            mess = doc.data['mess'];
            messageList = mess.reversed
                .take(20)
                .map<Widget>(
                  (s) => Message(
                      textEdit: messageController,
                      from: s['username'],
                      pictureUrl: s['pictureUrl'],
                      date: s['timeStamp'],
                      text: s['text'],
                      isMe: widget.user.email == s['email']),
                )
                .toList();
            return ListView.builder(
              reverse: true,
              controller: scrollController,
              itemCount: messageList.length,
              itemBuilder: (context, index) => messageList[index],
            );
          }
        }
      },
    );
  }
}

class SendButton extends StatelessWidget {
  const SendButton({Key key, this.callback}) : super(key: key);
  final VoidCallback callback;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Colors.deepOrange[900],
        borderRadius: BorderRadius.circular(5),
      ),
      width: size.width * .15,
      height: size.height * .075,
      child: FlatButton(
        onPressed: callback,
        child: Icon(Icons.keyboard_return, color: Colors.white),
      ),
    );
  }
}
