import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:popup_menu/popup_menu.dart';

class Message extends StatefulWidget {
  final String from, pictureUrl, text;
  final String date;
  final bool isMe;
  final TextEditingController textEdit;

  const Message(
      {Key key,
      this.from,
      this.pictureUrl,
      this.text,
      this.date,
      this.isMe,
      this.textEdit})
      : super(key: key);

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  PopupMenu menu;
  GlobalKey messageKey = GlobalKey();

  Color myColor = Colors.orange[800];
  Color otherColor = Colors.grey[700];

  @override
  void initState() {
    super.initState();
    menu = PopupMenu(
      backgroundColor: Colors.grey[900],
      maxColumn: 2,
      lineColor: Colors.grey[700],
      items: [
        MenuItem(
          image: Image.asset('images/copy.png', color: Colors.white),
          title: 'Copy',
          textStyle: TextStyle(color: Colors.white),
        ),
        MenuItem(
          image: Icon(Icons.reply, color: Colors.white),
          title: 'Reply',
          textStyle: TextStyle(color: Colors.white),
        )
      ],
      onClickMenu: onClickMenu,
      onDismiss: onDismiss,
    );
  }

  void onClickMenu(MenuItemProvider item) {
    if (item.menuTitle == "Copy")
      Clipboard.setData(ClipboardData(text: widget.text));
    else {
      // ignore: unnecessary_statements
      widget.isMe ? null : widget.textEdit.text = '@' + widget.from + " ";
    }
  }

  void onDismiss() {
    setState(() {
      widget.isMe
          ? myColor = Colors.orange[800]
          : otherColor = Colors.grey[700];
    });
  }

  @override
  Widget build(BuildContext context) {
    PopupMenu.context = context;

    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    ImageProvider image() {
      if (widget.pictureUrl == null) {
        return AssetImage('images/horn_ios.png');
      } else {
        return CachedNetworkImageProvider(widget.pictureUrl,
            errorListener: () {});
      }
    }

    Widget profilePic(bool me, ImageProvider img) {
      return Container(
          width: size.width * .12,
          height: size.width * .12,
          child: me
              ? SizedBox()
              : CircleAvatar(
                  backgroundColor: Colors.deepOrange[900],
                  backgroundImage: img,
                ));
    }

    Widget displayText(String text) {
      bool _isLink(text) {
        final matcher = new RegExp(
            r"(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)");
        return matcher.hasMatch(text);
      }

      if (_isLink(text)) {
        return GestureDetector(
          child: Linkify(
            onOpen: (link) async {
              if (await canLaunch(link.url)) {
                await launch(link.url);
              } else {}
            },
            text: text,
            style: TextStyle(
                color: widget.isMe ? Colors.white : Colors.white,
                fontSize: 15 * scaleFactor),
            linkStyle: TextStyle(
                color: Colors.lightBlue[200], fontSize: 15 * scaleFactor),
          ),
        );
      } else {
        return Text(text,
            style: TextStyle(
                color: widget.isMe ? Colors.white : Colors.white,
                fontSize: 15 * scaleFactor));
      }
    }

    return Container(
      color: Colors.transparent,
      child: Padding(
        padding: widget.isMe
            ? EdgeInsets.only(right: size.width * .015)
            : EdgeInsets.zero,
        child: Column(
          crossAxisAlignment:
              widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: widget.isMe
                  ? CrossAxisAlignment.end
                  : CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                // profile picture circle
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: size.width * .015),
                  child: GestureDetector(
                      onTap: () {
                        userProfile(context, widget.from, widget.pictureUrl);
                      },
                      child: profilePic(widget.isMe, image())),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: widget.isMe
                      ? CrossAxisAlignment.end
                      : CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: widget.isMe
                              ? EdgeInsets.symmetric(
                                  vertical: size.height * .002)
                              : EdgeInsets.only(
                                  bottom: size.height * .002,
                                  top: size.height * .006,
                                ),
                          child: widget.isMe

                              //username
                              ? SizedBox()
                              : Text(widget.from,
                                  style: TextStyle(
                                      color: Colors.cyan,
                                      fontWeight: FontWeight.w800)),
                        ),
                        Container(
                          constraints: BoxConstraints(
                              maxWidth: widget.isMe
                                  ? size.width * .75
                                  : size.width * .75),
                          decoration: BoxDecoration(
                            color: widget.isMe ? myColor : otherColor,
                            borderRadius: widget.isMe
                                ? BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                    bottomLeft: Radius.circular(20),
                                  )
                                : BorderRadius.only(
                                    topRight: Radius.circular(20),
                                    bottomLeft: Radius.circular(20),
                                    bottomRight: Radius.circular(20),
                                  ),
                          ),

                          //Text
                          child: Column(
                            children: <Widget>[
                              Padding(
                                key: messageKey,
                                padding: const EdgeInsets.all(10.0),
                                child: GestureDetector(
                                  onLongPress: () {
                                    setState(() {
                                      widget.isMe
                                          ? myColor = Colors.orange[900]
                                          : otherColor = Colors.grey[900];
                                    });

                                    menu.show(
                                      widgetKey: messageKey,
                                    );
                                  },
                                  child: displayText(widget.text),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Text(widget.date,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12 * scaleFactor,
                          fontWeight: FontWeight.w300,
                        ))
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void userProfile(BuildContext context, String userName, String pic) {
    showDialog(
        context: context,
        builder: (context) {
          Size size = MediaQuery.of(context).size;
          final scaleFactor = MediaQuery.of(context).textScaleFactor;

          Widget image(String picUrl) {
            if (picUrl == null) {
              return Image(
//                width: size.width * .6,
//                height: size.width * .6,
                image: AssetImage(
                  'images/horn_ios.png',
                ),
//                fit: BoxFit.fill,
              );
            } else {
              return CachedNetworkImage(
//                fit: BoxFit.fill,
                imageUrl: picUrl,
                useOldImageOnUrlChange: true,
                errorWidget: (context, url, error) =>
                    Image.asset('images/horn_ios.png'),
              );
            }
          }

          return Dialog(
            child: Container(
                width: size.width * .66,
                color: Theme.of(context).cardColor,
                child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      image(pic),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: size.height * .02),
                        child: Container(
                          child: Text(
                            userName,
                            style: TextStyle(
                                fontSize: 25 * scaleFactor,
                                color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                )),
          );
        });
  }
}
