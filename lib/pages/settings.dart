import 'package:cloud_functions/cloud_functions.dart';
import 'package:dragonhorn/pages/ImageHandler.dart';
import 'package:dragonhorn/pages/authentication.dart';
import 'package:dragonhorn/pages/myThemes.dart';
import 'package:dragonhorn/pages/optionProviders.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppSettingsScreen extends StatefulWidget {
  final FirebaseUser user;

  const AppSettingsScreen({Key key, this.user}) : super(key: key);
  @override
  _AppSettingsScreenState createState() => _AppSettingsScreenState();
}

class _AppSettingsScreenState extends State<AppSettingsScreen> {
  final myThemes = MyThemes();
  bool darkTheme = false;
  bool notiSound = false;

  @override
  Widget build(BuildContext context) {
    final options = Provider.of<OptionProvider>(context);

    final scaleFactor = MediaQuery.of(context).textScaleFactor;
    SharedPreferences.getInstance().then((pref) {
      setState(() {
        darkTheme = pref.getBool('theme');
      });
    });

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Theme.of(context).cardColor,
        title: Text('Settings',
            style: TextStyle(fontSize: 20 * scaleFactor, color: Colors.white)),
      ),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            ListTile(
              trailing: Switch(
                activeColor: Theme.of(context).accentColor,
                value: darkTheme,
                onChanged: (value) {
                  onThemeChanged(value, options);
                },
              ),
              leading: Icon(Icons.brightness_2,
                  color: Theme.of(context).primaryColor),
              title: Text('Dark Theme',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20 * scaleFactor)),
            ),
            Divider(),
            ListTile(
                leading: Icon(Icons.do_not_disturb_alt,
                    color: Theme.of(context).primaryColor),
                title: Text(
                  'No Adds',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20 * scaleFactor),
                )),
            ListTile(
                trailing: RaisedButton(
                  elevation: 3,
                  child: Text('Delete', style: TextStyle(color: Colors.white)),
                  onPressed: () => logOutButton(context, widget.user),
                ),
                leading: Icon(Icons.delete_forever,
                    color: Theme.of(context).primaryColor),
                title: Text(
                  'Delete Account',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20 * scaleFactor),
                )),
          ],
        ),
      ),
    );
  }

  void onThemeChanged(bool theme, OptionProvider op) async {
    (theme)
        ? op.setTheme(myThemes.darkTheme)
        : op.setTheme(myThemes.lightThemes);

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool('theme', theme);
  }

  void logOutButton(BuildContext context, FirebaseUser user) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Theme.of(context).backgroundColor,
          child: Container(
            decoration: BoxDecoration(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: size.height * .005),
                  alignment: Alignment.center,
                  color: Theme.of(context).cardColor,
                  child: Text(
                    'Deleting Account',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30 * scaleFactor,
                    ),
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(
                        right: size.width * .08,
                        left: size.width * .08,
                        top: size.height * .03,
                        bottom: size.height * .03),
                    child: Text(
                      'This action will permanently delete all data from this account.' +
                          "\n" +
                          'Do you still want to proceed?',
                      style: TextStyle(
                          color: Colors.red, fontSize: 20 * scaleFactor),
                    )),
                ButtonBar(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    FlatButton(
                      child: Text('Yes',
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 20 * scaleFactor)),
                      onPressed: () async {
                        Provider.of<ImageHandler>(context)
                            .deleteUrlAndImage(user);
                        final HttpsCallable callable = CloudFunctions.instance
                            .getHttpsCallable(functionName: 'deleteAccount');
                        callable.call().then((value) {
                          Provider.of<UserAuth>(context)
                              .refresh()
                              .whenComplete(() {
                            Navigator.of(context)
                                .popUntil(ModalRoute.withName('/'));
                          });
                        });
                      },
                    ),
                    FlatButton(
                      child: Text('No',
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 20 * scaleFactor)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
