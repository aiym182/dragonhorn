import 'package:flutter/material.dart';

class MyThemes {
  ThemeData lightThemes = ThemeData(
    fontFamily: 'WorkSans',
    appBarTheme: AppBarTheme(
        brightness: Brightness.light,
        color: Colors.white,
        textTheme: TextTheme(
          title: TextStyle(color: Colors.black, fontFamily: 'WorkSans'),
        ),
        iconTheme: IconThemeData(color: Colors.black)),
    primaryTextTheme: TextTheme(
      display1: TextStyle(color: Colors.black),
      subhead: TextStyle(color: Colors.black),
    ),
    cardColor: Colors.deepOrange[900],
    primaryColorLight: Colors.deepOrange[900],
    primaryColor: Colors.black,
    primaryColorDark: Colors.black54,
    accentColor: Colors.deepOrange[900],
    backgroundColor: Colors.white,
    brightness: Brightness.light,
    cursorColor: Colors.deepOrange[900],
    scaffoldBackgroundColor: Colors.white,
    buttonColor: Colors.deepOrange[900],
    hintColor: Colors.black38,
  );

  ThemeData darkTheme = ThemeData(
    fontFamily: 'WorkSans',
    appBarTheme: AppBarTheme(
      brightness: Brightness.dark,
      color: Colors.grey[900],
      textTheme: TextTheme(
          title: TextStyle(color: Colors.white, fontFamily: 'WorkSans')),
      iconTheme: IconThemeData(color: Colors.white),
    ),
    primaryTextTheme: TextTheme(
        display1: TextStyle(color: Colors.white),
        subhead: TextStyle(color: Colors.white)),
    cardColor: Colors.grey[900],
    primaryColor: Colors.white,
    primaryColorLight: Colors.white,
    primaryColorDark: Colors.white70,
    accentColor: Colors.deepOrange[900],
    brightness: Brightness.dark,
    backgroundColor: Colors.black,
    cursorColor: Colors.deepOrange[900],
    scaffoldBackgroundColor: Colors.grey[900],
    dividerColor: Colors.black,
    buttonColor: Colors.deepOrange[900],
    focusColor: Colors.deepOrange[900],
    hintColor: Colors.black38,
  );
}
