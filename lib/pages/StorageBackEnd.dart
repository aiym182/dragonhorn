import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';

class FirebaseBackEnd with ChangeNotifier {
  FirebaseBackEnd(this.name);

  Firestore db = Firestore.instance;
  String name;
  String savedname;

  void updateUserName(FirebaseUser user, String username) {
    try {
      db
          .collection('profile')
          .document(user.uid)
          .updateData({'username': username});
    } catch (e) {
      print(e.toString());
    }
  }

  void setUserData(FirebaseUser user, String username) {
    db.collection('profile').document(user.uid).setData({
      'username': username,
      'email': user.email,
      'pictureUrl': null,
    });
  }

  void updateUserData(FirebaseUser user, String username) {
    db.collection('profile').document(user.uid).updateData(
      {
        "username": username,
        "email": user.email,
      },
    );
    setLocalUserData(user.email, username);
  }

  Future<void> setLocalUserData(String email, username) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('email', email);
    pref.setString('username', username);
    setName(username);
    notifyListeners();
  }

  Future<void> setLocalUserName(String username) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('username', username);
  }

  Future<void> getLocalUserName() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    savedname = pref.getString('username');
  }

  Future<String> getOnlineUserName(FirebaseUser user) async {
    DocumentSnapshot doc =
        await db.collection('profile').document(user.uid).get();

    return await doc.data['username'];
  }

  Future<void> deleteUserDocument(FirebaseUser user) async {
    DocumentSnapshot doc =
        await db.collection('profile').document(user.uid).get();

    await doc.reference.delete();
  }

  Future<String> getOnlinePictureUrl(FirebaseUser user) async {
    DocumentSnapshot doc =
        await db.collection('profile').document(user.uid).get();
    return await doc.data['pictureUrl'];
  }

  updateOnDisplay(FirebaseUser user) {
    if (name == null) {
      getOnlineUserName(user).then((value) {
        setName(value);
        updateUserData(user, value);
        getLocalUserName();
      });
    } else {
      if (name != savedname) {
        getOnlineUserName(user).then((value) {
          savedname = value;
          setLocalUserName(savedname);
          setName(savedname);
          getLocalUserName();
        });
      } else {}
    }
  }

  getName() => name = name;
  setName(String username) {
    name = username;
    notifyListeners();
  }

  setSavedName(String username) {
    savedname = username;
    notifyListeners();
  }
}
