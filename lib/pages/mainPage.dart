import 'dart:async';
import 'package:dragonhorn/pages/ImageHandler.dart';
import 'package:dragonhorn/pages/StorageBackEnd.dart';
import 'package:flutter/material.dart';
import 'package:dragonhorn/pages/alert.dart';
import 'package:dragonhorn/pages/profileSetting.dart';
import 'package:dragonhorn/pages/settings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:dragonhorn/pages/authentication.dart';
import 'package:dragonhorn/pages/chat.dart';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';

class Choice {
  const Choice({
    this.title,
    this.icon,
  });
  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Alert', icon: Icons.notifications),
  const Choice(title: 'Chat', icon: Icons.chat_bubble),
];

class HomeWidget extends StatefulWidget {
  final FirebaseUser user;
  const HomeWidget({Key key, this.user}) : super(key: key);
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  String username;
  FirebaseMessaging fm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  @override
  void initState() {
    super.initState();
    fm.subscribeToTopic('all');

    fcmListeners();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    iosSubscription.cancel();
  }

  void fcmListeners() {
    if (Platform.isIOS) {
      fm.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
      iosSubscription = fm.onIosSettingsRegistered.listen((data) {});
    }

    fm.configure(
        onMessage: (Map<String, dynamic> message) async {
          String title;
          String body;

          if (Theme.of(context).platform == TargetPlatform.iOS) {
            setState(() {
              title = message['aps']['alert']['title'];
              body = message['aps']['alert']['body'];
            });
          } else {
            setState(() {
              title = message['notification']['title'];
              body = message['notification']['body'];
            });
          }
          Size size = MediaQuery.of(context).size;
          final scaleFactor = MediaQuery.of(context).textScaleFactor;
          showDialog(
            context: context,
            builder: (context) => Dialog(
              elevation: 5,
              backgroundColor: Theme.of(context).backgroundColor,
              child: Container(
                width: size.width * .7,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.symmetric(
                        vertical: size.height * .005,
                      ),
                      color: Theme.of(context).cardColor,
                      width: double.infinity,
                      child: Text(
                        title,
                        style: TextStyle(
                            color: Colors.white, fontSize: 28 * scaleFactor),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      color: Theme.of(context).backgroundColor,
                      width: double.infinity,
                      padding: EdgeInsets.only(
                        right: size.width * .08,
                        left: size.width * .08,
                        top: size.height * .03,
                        bottom: size.height * .03,
                      ),
                      child: Text(
                        body,
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18 * scaleFactor),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                          color: Colors.transparent,
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: size.width * .08,
                                vertical: size.height * .03),
                            child: Text('Ok',
                                style: TextStyle(
                                  fontSize: 20 * scaleFactor,
                                  color: Theme.of(context).accentColor,
                                )),
                          )),
                    )
                  ],
                ),
              ),
            ),
          );
        },
        onLaunch: (Map<String, dynamic> message) async {},
        onResume: (Map<String, dynamic> message) async {});
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    username = Provider.of<FirebaseBackEnd>(context).getName();
    Provider.of<FirebaseBackEnd>(context).updateOnDisplay(widget.user);

    return DefaultTabController(
      length: choices.length,
      child: Scaffold(
        drawer: handleDrawer(context, widget.user, username),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(size.height * .18),
          child: AppBar(
            iconTheme:
                IconThemeData(color: Theme.of(context).primaryColorLight),
            elevation: 1,
            title: Text(
              'Dragon Horn',
              style: TextStyle(
                color: Theme.of(context).primaryColorLight,
                fontSize: 30 * scaleFactor,
                fontWeight: FontWeight.w600,
              ),
            ),
            centerTitle: true,
            bottom: TabBar(
              labelColor: Theme.of(context).primaryColorLight,
//                  isScrollable: true,
              indicatorColor: Theme.of(context).primaryColorLight,
              tabs: choices.map((Choice choice) {
                return Tab(
                  text: choice.title,
                  icon: Icon(choice.icon),
                );
              }).toList(),
            ),
          ),
        ),
        body: TabBarView(
          children: [
            AlertScreen(),
            ChatScreen(
                user: widget.user,
                username: username,
                picUrl: Provider.of<ImageHandler>(context).getUrl()),
          ],
        ),
      ),
    );
  }
}

Widget nameUpdate(BuildContext context, FirebaseUser user, String username) {
  final scaleFactor = MediaQuery.of(context).textScaleFactor;

  if (username == null) {
    return Text("",
        style: TextStyle(fontSize: 30 * scaleFactor, color: Colors.white));
  } else {
    return Text(username,
        style: TextStyle(fontSize: 30 * scaleFactor, color: Colors.white));
  }
}

Widget handleDrawer(BuildContext context, FirebaseUser user, String username) {
  final scaleFactor = MediaQuery.of(context).textScaleFactor;

  return Drawer(
      child: Container(
    color: Theme.of(context).backgroundColor,
    child: MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Hello,",
                    style: TextStyle(
                        fontSize: 25 * scaleFactor, color: Colors.white)),
                nameUpdate(context, user, username),
              ],
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
            ),
          ),

          // Log In and Sign Up Button
          ListTile(
            title: Text('Profile',
                style: TextStyle(
                    fontSize: 20 * scaleFactor,
                    color: Theme.of(context).primaryColorLight)),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => ProfileSetting(user: user),
                ),
              );
            },
            leading:
                Icon(Icons.group, color: Theme.of(context).primaryColorLight),
          ),

          ListTile(
            title: Text('Settings',
                style: TextStyle(
                    fontSize: 20 * scaleFactor,
                    color: Theme.of(context).primaryColorLight)),
            onTap: () {
              Navigator.of(context).pop();

//              Navigator.of(context).popAndPushNamed('setting', arguments: user);

              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) => AppSettingsScreen(user: user)),
              );
            },
            leading: Icon(Icons.settings,
                color: Theme.of(context).primaryColorLight),
          ),
          ListTile(
            title: Text('Log Out',
                style: TextStyle(
                    fontSize: 20 * scaleFactor,
                    color: Theme.of(context).primaryColorLight)),
            onTap: () {
              Navigator.of(context).pop();
              LogoutButton(context);
            },
            leading: Icon(Icons.exit_to_app,
                color: Theme.of(context).primaryColorLight),
          ),
        ],
      ),
    ),
  ));
}

class LogoutButton {
  @override
  LogoutButton(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Theme.of(context).backgroundColor,
          child: Container(
            decoration: BoxDecoration(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: size.height * .005),
                  alignment: Alignment.center,
                  color: Theme.of(context).cardColor,
                  child: Text(
                    'Loggin Out',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30 * scaleFactor,
                    ),
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(
                        right: size.width * .08,
                        left: size.width * .08,
                        top: size.height * .03,
                        bottom: size.height * .03),
                    child: Text(
                      'Are you sure?',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 20 * scaleFactor),
                    )),
                ButtonBar(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    FlatButton(
                      child: Text('Yes',
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 20 * scaleFactor)),
                      onPressed: () async {
                        Provider.of<UserAuth>(context)
                            .signOut(context)
                            .whenComplete(() {
                          Provider.of<FirebaseBackEnd>(context)
                              .setSavedName(null);
                          Provider.of<FirebaseBackEnd>(context).setName(null);
                          Provider.of<ImageHandler>(context).setUrl(null);
                          Provider.of<ImageHandler>(context).setUpdated(false);
                          Provider.of<ImageHandler>(context)
                              .setPictureUrlToLocalData(null);

                          Navigator.of(context)
                              .popUntil(ModalRoute.withName('/'));
                        });
//                            .then((value) => Navigator.of(context).pop());
                      },
                    ),
                    FlatButton(
                      child: Text('No',
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 20 * scaleFactor)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
