import 'package:dragonhorn/pages/optionProviders.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:dragonhorn/pages/authentication.dart';
import 'package:provider/provider.dart';
import 'package:dragonhorn/pages/StorageBackEnd.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String userEmail, userPassword, userName, userConfirmPassword;

  @override
  Widget build(BuildContext context) {
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Theme.of(context).cardColor,
        title: Text(
          'Sign Up',
          style: TextStyle(fontSize: scaleFactor * 20, color: Colors.white),
        ),
      ),
      body: signUpScreenBody(),
    );
  }

  bool emailValidator(String email) {
    Pattern pat =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pat);
    if (email == null || !regExp.hasMatch(email)) {
      return false;
    } else {
      return true;
    }
  }

  bool passWordValidator(String password) {
    if (password == null || password.length < 8 || password.length > 16) {
      return false;
    } else {
      return true;
    }
  }

  bool usernameValidator(String username) {
    if (username == null || username.length < 3 || username.length > 16) {
      return false;
    } else {
      return true;
    }
  }

  bool confirmValidator(String confirmPassword) {
    if (confirmPassword == userPassword) {
      return true;
    } else {
      return false;
    }
  }

  String finalValidator(String email, username, password, confirmPassword) {
    if (!emailValidator(email)) {
      return "Email format is not valid.";
    } else if (!usernameValidator(username)) {
      return 'Your username must be in between 3~15 characters.';
    } else if (!passWordValidator(password)) {
      return 'Your password must be in between 8~15 characters.';
    } else if (!confirmValidator(confirmPassword)) {
      return 'Your password must match your previous password.';
    } else
      return '';
  }

  Widget lastMessage(String message) {
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    if (message == null) {
      return Text('', style: TextStyle(fontSize: 15 * scaleFactor));
    } else {
      return Text(
        message,
        style: TextStyle(color: Colors.red, fontSize: 15 * scaleFactor),
      );
    }
  }

  String message;

  Widget signUpScreenBody() {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    return Form(
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: size.width,
                height: size.height * .05,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.group,
                        size: 30 * scaleFactor,
                        color: Theme.of(context).primaryColor),
                    Text(" Create an account",
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 20 * scaleFactor,
                          fontWeight: FontWeight.bold,
                        )),
                  ],
                ),
              ),
              SizedBox(height: 5),

              //Email

              Container(
                width: size.width * .6,
                height: size.height * .07,
                child: TextField(
                  style: TextStyle(color: Colors.black),
                  onChanged: (value) {
                    this.setState(() {
                      userEmail = value;
                    });
                  },
                  cursorColor: Colors.deepOrange[900],
                  textAlignVertical: TextAlignVertical.bottom,
                  autocorrect: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.deepOrange[900],
                    )),
                    hintText: 'Email',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              SizedBox(height: 5),

              // username

              Container(
                width: size.width * .6,
                height: size.height * .07,
                child: TextField(
                  style: TextStyle(color: Colors.black),
                  onChanged: (value) {
                    this.setState(() {
                      userName = value;
                    });
                  },
                  cursorColor: Colors.deepOrange[900],
                  textAlignVertical: TextAlignVertical.bottom,
                  autocorrect: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.deepOrange[900],
                    )),
                    hintText: 'Username',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              SizedBox(height: 5),

              //password

              Container(
                width: size.width * .6,
                height: size.height * .07,
                child: TextField(
                  style: TextStyle(color: Colors.black),
                  onChanged: (value) {
                    this.setState(() {
                      userPassword = value;
                    });
                  },
                  cursorColor: Colors.deepOrange[900],
                  textAlignVertical: TextAlignVertical.bottom,
                  autocorrect: false,
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.deepOrange[900],
                    )),
                    hintText: 'Password',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              SizedBox(height: 5),

              //confirming password

              Container(
                width: size.width * .6,
                height: size.height * .07,
                child: TextField(
                  style: TextStyle(color: Colors.black),
                  onChanged: (value) {
                    this.setState(() {
                      userConfirmPassword = value;
                    });
                  },
                  cursorColor: Colors.deepOrange[900],
                  textAlignVertical: TextAlignVertical.bottom,
                  autocorrect: false,
                  obscureText: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                      color: Colors.deepOrange[900],
                    )),
                    hintText: 'Confirm password',
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              SizedBox(height: size.height * .02),

              //signup button

              Container(
                width: size.width * .6,
                height: size.height * .07,
                color: Colors.deepOrange[900],
                child: FlatButton(
                  child: Text(
                    'Sign Up',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                    ),
                  ),
                  onPressed: () async {
                    if (emailValidator(userEmail) &&
                        usernameValidator(userName) &&
                        passWordValidator(userPassword) &&
                        confirmValidator(userConfirmPassword)) {
                      FirebaseUser user = await Provider.of<UserAuth>(context)
                          .userRegistration(userEmail, userPassword, userName)
                          .catchError(
                        (e) {
                          setState(() {
                            message = e.message.toString();
                          });
                        },
                      );

                      if (user != null) {
                        Provider.of<FirebaseBackEnd>(context)
                            .setUserData(user, userName);
                        Provider.of<OptionProvider>(context)
                            .saveNotiSound(true);
                        user.sendEmailVerification().whenComplete(() {
                          EmailVerificationMessage(context, user.email);
                        });
                      }
                    } else {
                      setState(() {
                        message = finalValidator(userEmail, userName,
                            userPassword, userConfirmPassword);
                      });
                    }
                  },
                ),
              ),

              SizedBox(height: size.height * .02),

              Container(
                  alignment: Alignment.center,
                  width: size.width * .6,
                  child: lastMessage(message)),
            ],
          ),
        ),
      ),
    );
  }
}

class EmailVerificationMessage {
  final String email;
  @override
  EmailVerificationMessage(BuildContext context, this.email) {
    Size size = MediaQuery.of(context).size;
    final scaleFactor = MediaQuery.of(context).textScaleFactor;

    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            elevation: 5,
            backgroundColor: Theme.of(context).backgroundColor,
            child: Container(
              width: size.width * .6,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                      vertical: size.height * .005,
                    ),
                    color: Theme.of(context).cardColor,
                    width: double.infinity,
                    child: Text(
                      'Verification Required',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                  SizedBox(height: size.height * .03),
                  Container(
                    alignment: Alignment.center,
                    color: Theme.of(context).backgroundColor,
                    width: double.infinity,
                    padding: EdgeInsets.only(
                      right: size.width * .05,
                      left: size.width * .05,
                      top: size.height * .03,
                      bottom: size.height * .03,
                    ),
                    child: Text(
                      "Verification email has been sent to " +
                          email +
                          "." +
                          "\n" +
                          "Please check your email.",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 15 * scaleFactor),
                    ),
                  ),
                  Container(
                    width: size.width * .4,
                    height: size.height * .06,
                    color: Theme.of(context).buttonColor,
                    child: FlatButton(
                      child: Text(
                        'Ok',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17 * scaleFactor,
                        ),
                      ),
                      onPressed: () async {
                        Navigator.of(context)
                            .popUntil(ModalRoute.withName('/'));
                      },
                    ),
                  ),
                  SizedBox(height: size.height * .03),
                ],
              ),
            ),
          );
        });
  }
}
