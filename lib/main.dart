import 'package:dragonhorn/pages/ImageHandler.dart';
import 'package:dragonhorn/pages/NotificationHandler.dart';
import 'package:dragonhorn/pages/profileSetting.dart';
import 'package:dragonhorn/pages/settings.dart';
import 'package:dragonhorn/pages/signup.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'pages/chatRoom.dart';
import 'pages/login.dart';
import 'pages/splash.dart';
import 'pages/mainPage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:dragonhorn/pages/authentication.dart';
import 'package:dragonhorn/pages/StorageBackEnd.dart';
import 'package:dragonhorn/pages/optionProviders.dart';
import 'package:dragonhorn/pages/myThemes.dart';

void main() {
  bool darkMode = false;
  bool notiSound = true;
  String name = '';

  SharedPreferences.getInstance().then((pref) {
    if (pref.getString('SavedName') == null) {
      pref.setString('SavedName', '');
    } else {
      name = pref.getString('SavedName');
    }

    if (pref.getBool('theme') == null) {
      darkMode = false;
      pref.setBool('theme', darkMode);
    } else {
      darkMode = pref.getBool('theme');
    }
    runApp(AppTitle(darkmode: darkMode, notiSound: notiSound, name: name));
  });
}

class AppTitle extends StatefulWidget {
  const AppTitle({
    Key key,
    this.darkmode,
    this.notiSound,
    this.name,
  }) : super(key: key);

  @override
  _AppTitleState createState() => _AppTitleState();

  final bool darkmode;
  final bool notiSound;
  final String name;
}

class _AppTitleState extends State<AppTitle> {
  MyThemes myThemes = MyThemes();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserAuth>(create: (_) => UserAuth()),
        ChangeNotifierProvider<FirebaseBackEnd>(
          create: (_) => FirebaseBackEnd(widget.name),
        ),
        ChangeNotifierProvider<ImageHandler>(
          create: (_) => ImageHandler(),
        ),
        ChangeNotifierProvider<NotificationHandler>(
            create: (_) => NotificationHandler()),
        ChangeNotifierProvider<OptionProvider>(
            create: (_) => OptionProvider(
                widget.darkmode ? myThemes.darkTheme : myThemes.lightThemes)),
      ],
      child: MaterialAppWithTheme(),
    );
  }
}

class MaterialAppWithTheme extends StatelessWidget {
  const MaterialAppWithTheme({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<OptionProvider>(context);

    return MaterialApp(
      title: 'Dragon Horn',
      routes: <String, WidgetBuilder>{
        'home': (BuildContext context) => MaterialAppWithTheme(),
        'login': (BuildContext context) => Login(),
        'mainScreen': (BuildContext context) => HomeWidget(),
        'signUp': (BuildContext context) => SignUpScreen(),
        'profile': (BuildContext context) => ProfileSetting(),
        'setting': (BuildContext context) => AppSettingsScreen(),
        'chatRoom': (BuildContext context) => ChatRoom(),
        'splash': (BuildContext context) => SplashScreen(),
      },
      home: handleWindowDisplay(),
      theme: themeChanger.getTheme(),
    );
  }
}

Widget handleWindowDisplay() {
  return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return SplashScreen();
        } else {
          if (!snapshot.hasData) {
            return Login();
          } else {
            FirebaseUser user = snapshot.data;
            if (user.isEmailVerified) {
              var _imageHandler = Provider.of<ImageHandler>(context);

              if (_imageHandler.picUrl == null) {
                _imageHandler.getOnlinePictureUrl(user).then((value) {
                  if (value == null) {
                    _imageHandler.setPictureUrlToLocalData(null);
                    _imageHandler.onlinePicUrl = null;
                  } else {
                    _imageHandler.setPictureUrlToLocalData(value);
                    _imageHandler.onlinePicUrl = value;
                  }
                }).whenComplete(() {
                  _imageHandler.getAndSetLocalPictureUrl(user);
                });
              }
              return HomeWidget(user: user);
            } else {
              Provider.of<UserAuth>(context).signOut(context);
              return Login();
            }
          }
        }
      });
}
